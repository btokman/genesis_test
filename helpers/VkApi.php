<?php

namespace helpers;

use Psr\Log\InvalidArgumentException;

/**
 * Class VkApi
 * @package helpers
 */
class VkApi
{
    /** Api url */
    const API_URL = 'https://api.vk.com/method/';
    /** Allowed api methods */
    const ALLOWED_METHODS = ['photos.getAlbums', 'photos.get'];
    /** Application service key */
    const SERVICE_KEY = 'c48e0c3ec48e0c3ec48e0c3e4ec4dee654cc48ec48e0c3e9fa772432e9406f313f905d0';
    /** Vk api version */
    const API_VERSION = 5.63;
    /** @link http://spys.one/socks/ */
    const PROXY = 'socks5://188.120.255.240:63748';

    public $curl;

    /**
     * VkApi constructor.
     * @param string $apiMethod
     * @param array $params
     */
    public function __construct(string $apiMethod, array $params = [])
    {
        if (!in_array($apiMethod, self::ALLOWED_METHODS)) {
            throw new InvalidArgumentException('Api method not supported');
        }

        $url = self::buildApiUrl($apiMethod, $params);

        $this->curl = $this->initCurl($url);
    }


    /**
     * Init curl set options {TYPE : GET ,Content type : json , Return transfer : true }
     * @param $url
     * @return resource
     */
    public function initCurl(string $url)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
        curl_setopt($curl, CURLOPT_PROXY, self::PROXY);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
        ]);

        return $curl;
    }

    /**
     * Build api url
     * @param string $method
     * @param array $params
     * @return string
     */
    public static function buildApiUrl(string $method, array $params)
    {
        $url = self::API_URL . "$method?access_token=" . self::SERVICE_KEY . "&v=" . self::API_VERSION . '&';

        foreach ($params as $key => $value) {
            $url .= end($params) === $value ? "$key=$value" : "$key=$value&";
        }

        return $url;
    }

    /**
     * Execute cUrl request
     * @return mixed
     */
    public function execute()
    {
        $response = curl_exec($this->curl);;
        curl_close($this->curl);;
        return json_decode($response);
    }
}