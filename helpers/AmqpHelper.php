<?php

namespace helpers;

use PhpAmqpLib\Connection\AMQPStreamConnection;

/**
 * Class AmqpHelper
 * @package helpers
 */
class AmqpHelper
{
    const DEFAULT_QUEUE = 'queue';

    /**
     * @return AMQPStreamConnection
     */
    public static function getConnection()
    {
        $config = (require './config/config.php')['rabbitMq'];
        $connection = new AMQPStreamConnection($config['host'], $config['port'], $config['user'], $config['password']);

        return $connection;
    }
}