<?php

namespace migrations;

use Illuminate\Database\Capsule\Manager;

/**
 * Class UserAlbumsTable
 * @package migrations
 */
class UserAlbumsTable
{
    public static function up()
    {
        Manager::schema()->create('user_albums', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('album_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }
}