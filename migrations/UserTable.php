<?php

namespace migrations;

use Illuminate\Database\Capsule\Manager;


/**
 * Class UserTable
 * @package migrations
 */
class UserTable
{
    public static function up()
    {
        Manager::schema()->create('users', function ($table) {
            $table->increments('id');
            $table->string('vk_id');
            $table->timestamps();
        });
    }
}