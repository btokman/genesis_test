<?php

namespace migrations;

use Illuminate\Database\Capsule\Manager;

/**
 * Class UserAlbumsTable
 * @package migrations
 */
class UserPhotosTable
{
    public static function up()
    {
        Manager::schema()->create('user_photos', function ($table) {
            $table->increments('id');
            $table->string('photo_src');
            $table->integer('album_id')->unsigned();
            $table->foreign('album_id')->references('id')->on('user_albums')->onDelete('cascade');;
        });
    }
}