<?php

namespace senders;

use helpers\AmqpHelper;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class DataSender
 * @package senders
 */
class DataSender
{
    /**
     * Send data to RabbitMq
     * @param $data
     */
    public function execute($data)
    {
        $connection = AmqpHelper::getConnection();
        $channel = $connection->channel();

        $channel->queue_declare(AmqpHelper::DEFAULT_QUEUE, false, true, false, false);

        $msg = new AMQPMessage($data);

        $channel->basic_publish($msg, '', 'queue');

        $channel->close();
        $connection->close();
    }
}