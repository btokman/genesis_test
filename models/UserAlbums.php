<?php

namespace models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserAlbums
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $album_id
 *
 * @package models
 */
class UserAlbums extends Model
{
    protected $table = 'user_albums';

    protected $fillable = ['user_id', 'album_id'];

    public $timestamps = false;

    /**
     * Load data to the models
     * @param $userId
     * @param $albumId
     * @return UserAlbums
     */
    public static function processData($userId, $albumId)
    {
        $album = self::updateOrCreate(['user_id' => $userId, 'album_id' => $albumId], ['user_id' => $userId, 'album_id' => $albumId]);
        return $album;
    }

    /**
     * Get users (inverse relation)
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('models\User');
    }

    /**
     * Get related photos
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function photos()
    {
        return $this->hasMany('models\UserPhotos', 'album_id');
    }

}