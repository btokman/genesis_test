<?php

namespace models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 *
 * @property  integer $id
 * @property string $vk_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @package models
 */
class User extends Model
{
    protected $table = 'users';

    protected $fillable = ['vk_id'];

    /**
     * @param $id
     * @return mixed
     */
    public static function getByVkId($id)
    {
        return self::where('vk_id', $id)->first();
    }

    /**
     * Get related albums
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function albums()
    {
        return $this->hasMany('models\UserAlbums');
    }
}