<?php

namespace models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserPhotos
 *
 * @property integer $id
 * @property string $photo_src
 * @property integer $album_id
 *
 * @package models
 */
class UserPhotos extends Model
{
    protected $table = 'user_photos';

    protected $fillable = ['photo_src', 'album_id'];

    public $timestamps = false;

    /**
     * Load data to the models
     * @param $albumId
     * @param $photoSrc
     */
    public static function processData($albumId, $photoSrc)
    {
        self::updateOrCreate(['album_id' => $albumId, 'photo_src' => $photoSrc], ['photo_src' => $photoSrc]);
    }

    /**
     * Get albums (inverse relation)
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function albums()
    {
        return $this->belongsTo('models\UserAlbums', 'album_id');
    }
}