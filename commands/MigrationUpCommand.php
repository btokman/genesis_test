<?php

namespace commands;

use migrations\{
    UserAlbumsTable, UserTable, UserPhotosTable
};

use Symfony\Component\Console\{
    Command\Command, Input\InputInterface, Output\OutputInterface
};

/**
 * Class MigrationUpCommand
 * @package commands
 */
class MigrationUpCommand extends Command
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('migrate')
            ->setDescription('Up mysql migrations');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            UserTable::up();
            UserAlbumsTable::up();
            UserPhotosTable::up();
            $output->writeln(['Migration successful done']);

        } catch (\PDOException $e) {
            var_dump($e->getMessage());
        }

    }
}