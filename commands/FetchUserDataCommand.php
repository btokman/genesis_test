<?php

namespace commands;

use senders\DataSender;
use Symfony\Component\Console\{
    Command\Command, Input\InputInterface, Output\OutputInterface, Question\Question
};

/**
 * Class FetchUserDataCommand
 * @package services
 */
class FetchUserDataCommand extends Command
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('fetch-user-data')
            ->setDescription('Fetch data from VK Api by user id');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['Welcome']);

        $helper = $this->getHelper('question');

        $question = new Question('Enter user id or file path (.csv): ', '');

        $question->setValidator(function ($answer) {
            if (strlen($answer) > 0) return $answer;
            throw new \RuntimeException('The value cannot be blank');
        });

        $question->setMaxAttempts(2);

        $identity = ['owner_id' => $helper->ask($input, $output, $question)];

        if (file_exists($identity['owner_id']) && pathinfo($identity['owner_id'])['extension'] === 'csv') {
            $csv = array_map('str_getcsv', file($identity['owner_id']));

            $identity = [];

            foreach ($csv as $item) {
                $identity[] = ['owner_id' => $item[0]];
            }

        }

        $identity = json_encode($identity);

        $sender = new DataSender();
        $sender->execute($identity);
    }
}