<?php

namespace commands;

use models\User;
use Symfony\Component\Console\{
    Command\Command, Helper\Table, Input\InputInterface, Output\OutputInterface, Question\Question
};

/**
 * Class ShowUserDataCommand
 * @package commands
 */
class ShowUserDataCommand extends Command
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('show-user-data')
            ->setDescription('Show users albums and photos');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['Welcome']);

        $helper = $this->getHelper('question');

        $question = new Question('Enter user id (default show all)', '');

        $identity = $helper->ask($input, $output, $question);

        $users = !strlen($identity) ? User::all() : [User::getByVkId($identity)];

        $table = new Table($output);
        $table->setHeaders(['User id', 'Album id', 'Photos']);

        $rows = [];

        foreach ($users as $user) {
            foreach ($user->albums as $album) {
                foreach ($album->photos as $key => $photo)
                    $rows[] = $key === 0 ? [$user->vk_id, $album->album_id, $photo->photo_src] : [
                        '', '', $photo->photo_src
                    ];
            }
        }

        $table->setRows($rows);
        $table->render();
    }
}