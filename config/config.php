<?php
return [
    'mysql' => [
        'driver' => 'mysql',
        'host' => 'localhost',
        'database' => 'genesis',
        'username' => 'root',
        'password' => 'root',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ]
    ,
    'rabbitMq' => [
        'host' => 'localhost',
        'port' => 5672,
        'user' => 'guest',
        'password' => 'guest'
    ]
];