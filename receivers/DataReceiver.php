<?php

namespace receivers;

use helpers\{
    AmqpHelper, VkApi
};
use models\{
    User, UserAlbums, UserPhotos
};
use PhpAmqpLib\Message\AMQPMessage;
use Illuminate\Database\Capsule\Manager as DB;

/**
 * Class DataReceiver
 * @package receivers
 */
class DataReceiver
{
    /**
     * Listen rabbitMq server
     */
    public function listen()
    {
        $connection = AmqpHelper::getConnection();
        $channel = $connection->channel();

        $channel->queue_declare(AmqpHelper::DEFAULT_QUEUE, false, true, false, false);

        $channel->basic_consume(AmqpHelper::DEFAULT_QUEUE, '', false, true, false, false,
            [$this, 'processData']);

        while (count($channel->callbacks)) $channel->wait();

        $channel->close();
        $connection->close();
    }

    /**
     * Process message
     * @param AMQPMessage $msg
     */
    public function processData(AMQPMessage $msg)
    {
        $data = json_decode($msg->body);

        if (count($data) === 1) {
            $this->bindData($data->owner_id);
            return;
        }

        foreach ($data as $item) {
            if (!isset($item->owner_id)) continue;
            $this->bindData($item->owner_id);
        }
    }

    /**
     * Save data
     * @param $ownerId
     */
    private function bindData($ownerId)
    {
        $api = new VkApi('photos.getAlbums', ['owner_id' => $ownerId]);
        $albumData = $api->execute();

        $items = $albumData->response->items ?? [];

        if (!count($items)) return;

        self::startTransaction($ownerId, $items);
    }

    /**
     * Insert datat to db
     * @param $ownerId
     * @param $items
     */
    private static function startTransaction($ownerId, $items)
    {
        try {
            DB::beginTransaction();

            $user = User::updateOrCreate(['vk_id' => $ownerId]);

            foreach ($items as $item) {

                if (!$item->size) continue;

                $album = UserAlbums::processData($user->getKey(), $item->id);

                $api = new VkApi('photos.get', ['owner_id' => $ownerId, 'album_id' => $album->album_id]);;
                $photoData = $api->execute();

                if (!isset($photoData->response)) return;

                foreach ($photoData->response->items as $photoItem) {
                    UserPhotos::processData($album->getKey(), $photoItem->photo_604);
                }
            }

            $user->touch();
            DB::commit();

        } catch (\PDOException $e) {
            DB::rollBack();
        }
    }
}