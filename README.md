 Installation

```sh
  $ git clone https://btokman@bitbucket.org/btokman/genesis_test.git
```


```sh
  $ composer install
```

 Change mysql/rabbitmq connection config in file `config\config.php`
 
 Up migrations

```sh
  $ ./app migrate
```

 Subscribe consumer
 
```sh
   $ ./consumer
```

 Fetch user data by user vk id (or read from csv file)
 
```sh
    $ ./app fetch-user-data
```
   
 Show user data by user id (default show all)
 
```sh
     $ ./app show-user-data
```